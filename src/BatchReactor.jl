module BatchReactor

using DifferentialEquations     # for solving the system of ODEs
using Fresa                     # for optimization 
using Printf                    # for formatted output
using PyPlot                    # for plotting profiles

const tfinal = 1.0              # no units given in paper
const Tmin = 298.0              # Kelvin
const Tmax = 398.0              # Kelvin

abstract type TemperatureProfile end

mutable struct PiecewiseLinearProfile <: TemperatureProfile
    ft :: Array{Float64}        # time deltas
    fT :: Array{Float64}        # temperature deltas
    T0 :: Float64               # initial temperature
    function PiecewiseLinearProfile(ft, fT, T0)
        if length(ft) != length(fT)
            error("Number of time points and temperature changes must match.")
        end
        if T0 < Tmin || T0 > Tmax
            error("T0 = $(T0) not in [$Tmin,$Tmax]")
        end
        new(ft,fT,T0)
    end
    function PiecewiseLinearProfile(t :: PiecewiseLinearProfile)
        new(t.ft, t.fT, t.T0)
    end
end
# allow up to 10 degrees change per time interval
ΔT_max = 10.0

function temperature(profile :: PiecewiseLinearProfile, t :: Float64) :: Float64
    τ = 0
    T = profile.T0
    for i=1:length(profile.ft)
        Δτ = profile.ft[i] * (tfinal - τ)
        ΔT = profile.fT[i] * ΔT_max
        if t < τ + Δτ
            # partial step to reach desired time
            T += ΔT * (t-τ)/Δτ
            # correct T if out of bounds
            if T < Tmin
                T = Tmin
            elseif T>Tmax
                T = Tmax
            end
            # leave loop
            break
        else
            # take full step
            T += ΔT
            # correct T if out of bounds
            if T < Tmin
                T = Tmin
            elseif T>Tmax
                T = Tmax
            end
        end
        τ += Δτ
    end
    return T
end

function Base.show(io::IO, profile::BatchReactor.PiecewiseLinearProfile)
    @printf(io, "PWL | %.4g ", profile.T0)
    for i = 1:length(profile.ft)
        @printf(io, "| %.4g ", profile.ft[i])
    end
    for i = 1:length(profile.fT)
        @printf(io, "| %.4g ", profile.fT[i])
    end
end

mutable struct QuadraticSplineProfile <: TemperatureProfile
    T0 :: Float64               # initial temperature
    Tf :: Float64               # final temperature
    t1 :: Float64               # time point 
    a :: Float64                # coefficients of quadratics
    b :: Float64
    c :: Float64
    d :: Float64
    e :: Float64
    f :: Float64
    function QuadraticSplineProfile(T0, Tf, t1)
        a = T0
        b = 0
        c = (Tf-T0)/t1
        d = (Tf*t1 - T0) / (t1 - 1)
        e = (2*T0 - 2*Tf) / (t1 - 1)
        f = (Tf - T0) / (t1 - 1)
        new(T0, Tf, t1, a, b, c, d, e, f)
    end
end

function temperature(p :: QuadraticSplineProfile, t :: Float64) :: Float64
    if t <= p.t1
        T = p.a + p.b*t + p.c*t^2
    else
        T = p.d + p.e*t + p.f*t^2
    end
    return T
end

function Base.show(io :: IO, profile :: BatchReactor.QuadraticSplineProfile)
    @printf(io, "QSP | %.3g | %.3g | %6.5g ", profile.T0, profile.Tf, profile.t1)
end

function plot( profile :: TemperatureProfile )
    n = 1000
    δx = 1.0/1000
    x = [(i-1)*δx for i in 1:n+1]
    y = [temperature(profile,x[i]) for i in 1:n+1]

    PyPlot.plot(x,y, linewidth=2.0, linestyle="--", )
    PyPlot.ylabel("Temperature (K)")
    PyPlot.xlabel("Time")
    PyPlot.title("Temperature profile")
end

function reactor(x :: Array{Float64,1},
                 p :: TemperatureProfile,
                 t :: Float64
                 ) :: Array{Float64,1}
    T = temperature(p,t)
    k = rates(T)
    # println("$t $T $k")
    return [-k[1] * x[1]^2
            k[1] * x[1]^2 - k[2] * x[2]
            k[2] * x[2]]
end

x0 = [1.0, 0.0, 0.0]

rates = T -> [4000.0 * exp(-2500.0/T)
              620000 * exp(-5000.0/T)]

function simulation(profile :: TemperatureProfile)
    # for testing some performance aspects, we have two ways of
    # simulating the reactor, i.e. solving the differential equations:
    # an efficient solver (RK 2-3) and a computationally inefficient
    # one, Euler's method
    efficient = true
    if efficient
        # efficient solver
        tspan = (0.0,tfinal)
        prob = ODEProblem(reactor, x0, tspan, profile)
        results = DifferentialEquations.solve(prob)
        results[end]
    else
        δt = tfinal/1e4
        t = 0.0
        x = x0
        while t < tfinal
            if t+δt > tfinal
                δt = tfinal - t
            end
            δx = δt * reactor(x, profile, t)
            t = t + δt
            x = x + δx
        end
        x                       # end values
    end
end

function objective(profile :: TemperatureProfile)
    results = simulation(profile)
    # take the last concentration of species B, times -1, as the first
    # objective function value and the last concentration of C as the
    # second objective function values.  Also indicate that the design
    # is always feasible.
    ([-results[2], results[3]], 0.0)
end

function Fresa.neighbour(x :: PiecewiseLinearProfile, f, domain)
    a = domain.lower(x)
    b = domain.upper(x)
    ft = Fresa.neighbour(x.ft, f, Fresa.Domain(x->a.ft, x->b.ft))
    fT = Fresa.neighbour(x.fT, f, Fresa.Domain(x->a.fT, x->b.fT))
    T0 = Fresa.neighbour(x.T0, f, Fresa.Domain(x->a.T0, x->b.T0))
    PiecewiseLinearProfile(ft,fT,T0)
end

function Fresa.neighbour(x :: QuadraticSplineProfile, f, domain)
    a = domain.lower(x)
    b = domain.upper(x)
    T0 = Fresa.neighbour(x.T0, f, Fresa.Domain(x->a.T0, x->b.T0))
    Tf = Fresa.neighbour(x.Tf, f, Fresa.Domain(x->a.Tf, x->b.Tf))
    t1 = Fresa.neighbour(x.t1, f, Fresa.Domain(x->a.t1, x->b.t1))
    QuadraticSplineProfile(T0, Tf, t1)
end

function solve(p0, domain :: Fresa.Domain)
    Fresa.solve(
        # the first 4 arguments are required
        objective,               # the objective function
        p0;                      # an initial point in the design space
        # the rest are option arguments for Fresa
        domain = domain,         # search domain for the decision variables
        archiveelite = false,    # save thinned out elite members
        elite = true,            # elitism by default
        ϵ = 0.001,               # tolerance for similarity detection
        fitnesstype = :hadamard, # how to rank solutions in multi-objective case
        #fitnesstype = :borda,   # how to rank solutions in multi-objective case
        multithreading = true,   # use multiple threads, if available
        ngen = 100,              # number of generations
        np = 20,                 # population size
        nrmax = 5,               # number of runners maximum
        ns = 100,                # number of stable solutions for stopping
        output = 1)              # how often to output information
end

function Fresa.randompoint(a :: PiecewiseLinearProfile, b :: PiecewiseLinearProfile)
    PiecewiseLinearProfile(Fresa.randompoint(a.ft, b.ft),
                           Fresa.randompoint(a.fT, b.fT),
                           Fresa.randompoint(a.T0, b.T0))
end
function Fresa.randompoint(a :: QuadraticSplineProfile, b :: QuadraticSplineProfile)
    QuadraticSplineProfile(Fresa.randompoint(a.T0, b.T0),
                           Fresa.randompoint(a.Tf, b.Tf),
                           Fresa.randompoint(a.t1, b.t1))
end

end
