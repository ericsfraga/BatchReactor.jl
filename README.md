# Batch reactor Fresa tutorial
This [Julia](https://julialang.org/) package is the basis for a [tutorial](https://www.ucl.ac.uk/~ucecesf/Fresa/tutorial.html) on the use of [Fresa](https://www.ucl.ac.uk/~ucecesf/fresa.html) for solving a problem in dynamic optimization for a chemical reactor operating temperature profile.
