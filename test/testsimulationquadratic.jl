using BatchReactor
profile = BatchReactor.QuadraticSplineProfile(350.0, 290.0, 0.5)
results = BatchReactor.simulation(profile)
using PyPlot
PyPlot.plot(results.t, [results.u[i][1] for i=1:length(results.u)])
PyPlot.plot(results.t, [results.u[i][2] for i=1:length(results.u)])
PyPlot.plot(results.t, [results.u[i][3] for i=1:length(results.u)])
PyPlot.xlabel("t")
PyPlot.ylabel("Concentration")
PyPlot.savefig("testsimulationquadratic.pdf")
