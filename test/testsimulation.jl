using BatchReactor
# define a temperature profile
profile = BatchReactor.PiecewiseLinearProfile(
    [0.5 1.0],  # half the time in first interval
    [0.0 -0.5], # constant first interval, down by half amount allowed in second
    398.0)      # use the maximum temperature allowed as initial temperature
# simulate the process
results = BatchReactor.simulation(profile)
println("Results from simulation: $results")
# # plot out the results, i.e. the concentrations of all species as a
# # function of time.
# using PyPlot
# PyPlot.plot(results.t, [results.u[i][1] for i=1:length(results.u)])
# PyPlot.plot(results.t, [results.u[i][2] for i=1:length(results.u)])
# PyPlot.plot(results.t, [results.u[i][3] for i=1:length(results.u)])
# PyPlot.xlabel("t")
# PyPlot.ylabel("Concentration")
# PyPlot.savefig("testsimulation.pdf")
